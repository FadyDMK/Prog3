﻿using Shells.Model;
using System;

namespace Shells
{
    class Program
    {
        static void Main(string[] args)
        {
            //Abstract class can not be instantiated, results compilation error
            // Cylinder c1 = new Cylinder(9, 6);
            Rod r1 = new Rod(10, 3, 5);
            Rod r2 = new Rod(12, 3, 5);
            Tube t1 = new Tube(12, 3, 5, 2);

            Console.WriteLine(r1);
            Console.WriteLine(r2);
            Console.WriteLine(t1);
        }
    }
}
