﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaOrder
{
    class PizzaListParser : IPizzaParser
    {
        public List<Pizza> Parse(string multiLineText)
        {
            List<Pizza> loaded = new List<Pizza>();

            string[] lines = multiLineText.Split(
                new[] { Environment.NewLine },
                StringSplitOptions.None
            );

            foreach (var line in lines)
            {
                string[] words = line.Split(';');

                if (words.Length == 3)
                {
                    loaded.Add(
                        new Pizza(
                            words[0],
                            int.Parse(words[1]),
                            int.Parse(words[2])
                        )
                    );
                }
            }

            return loaded;
        }
    }
}
