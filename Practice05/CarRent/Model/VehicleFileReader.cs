﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CarRent.Model
{
    internal class VehicleFileReader : IVehicleReader
    {
        private Parser parser;
        private int linesOfObject;

        public VehicleFileReader(Parser parser, int linesOfObject)
        {
            this.parser = parser;
            this.linesOfObject = linesOfObject;
        }

        public List<Vehicle> Read(string sourceName)
        {
            StreamReader reader = null;
            List<Vehicle> vehicles = new List<Vehicle>();

            StringBuilder newContent;
            try
            {
                reader = new StreamReader(sourceName);
                while (!reader.EndOfStream)
                {
                    newContent = new StringBuilder();
                    for(int l=0; l < linesOfObject; l++)
                    {
                        newContent.AppendLine(reader.ReadLine());
                    }

                    vehicles.Add(parser.Parse(newContent.ToString(), vehicles.Count + 1));
                }
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return vehicles;
        }
    }
}
