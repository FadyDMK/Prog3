﻿using Retoxi.Model;
using System;
using System.Windows.Forms;

namespace Retoxi
{
    public partial class MainForm : Form
    {
        private DrinkStorage storage = new DrinkStorage();

        public MainForm()
        {
            InitializeComponent();
        }

        #region Menu event handlers

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReadFile();

            EnableUI();
        }

        private void itallapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowItallap();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Was made on 09th practice of Programming 3 course", "About");
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HelpForm helpForm = new HelpForm();

            //Result of dialog window received and handled
            if (helpForm.ShowDialog() == DialogResult.OK)
            {
                MessageBox.Show("Good!");
            }
            else
            {
                MessageBox.Show("Pay more attention to closing!");
            }

            //Get and use data from HelpForm instance
            switch (helpForm.DidHelp)
            {
                case HelpForm.HelpUsefulness.Yes:
                    MessageBox.Show("I am glad the help helped! :)");
                    break;
                case HelpForm.HelpUsefulness.No:
                    MessageBox.Show("I am sad the help did not help! :(");
                    break;
                case HelpForm.HelpUsefulness.NotSelected:
                    MessageBox.Show("I don't know if the help helped! :|");
                    break;
            }
        }

        private void galeryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GalleryForm galleryForm = new GalleryForm();
            galleryForm.ShowDialog();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                DrinkPrinter.PrintListToFile(
                    storage, 
                    saveFileDialog1.FileName, 
                    DrinkPrinter.PrintToInventory
                );
            }
        }

        #endregion

        private void ReadFile()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    storage.Clear();
                    storage.AddRange(
                        new Loader<Drink>().LoadFile(
                            openFileDialog1.FileName, new DrinkParser()
                        )
                    );
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        String.Format("Error while reading the file! {0}", ex.Message),
                        "Attention!"
                    );
                }
            }
        }

        private void EnableUI()
        {
            bool success = storage.Count > 0;
            itallapToolStripMenuItem.Enabled = success;
            saveToolStripMenuItem.Enabled = success;

            if (success)
            {
                ShowItallap();
            }
        }

        private void ShowItallap()
        {
            DrinkMenuForm itallapForm = new DrinkMenuForm(storage);
            itallapForm.ShowDialog();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(MessageBox.Show("Are you sure to close the window?", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
