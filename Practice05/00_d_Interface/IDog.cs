﻿namespace _00_d_Interface
{
    internal interface IDog : IAnimal
    {
        void Bark();
    }
}
