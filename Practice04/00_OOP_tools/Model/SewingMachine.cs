﻿using System;

namespace _00_OOP_tools.Model
{
    public class SewingMachine : Machine
    {
        public SewingMachine(int width, int length, int height) : base(width, length, height)
        {
        }

        // When trying to hide method, compilation error is received,
        // because class has no implementation for inherited method
        // Indeed there is nothing to hide...
        public override void CreateWorkpiece()
        {
            Console.WriteLine("Sew the workpiece");
        }
    }
}
