﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Database
{
    class DBManager
    {
        IDbConnector connector;

        public DBManager(IDbConnector connector)
        {
            this.connector = connector;
        }

        public void CreateTable()
        {
            connector.Open();
            connector.GetCreateCommand().ExecuteNonQuery();
            connector.Close();
        }

        public void LoadFile(
            string fileName, 
            Action<int> progressReporter
        )
        {
            progressReporter(0);

            connector.Open();

            progressReporter(10);

            if (fileName.Length > 0)
            {
                LoadFile(fileName, ';', connector.GetInsertCommand(), progressReporter);
            }

            progressReporter(90);

            connector.Close();

            progressReporter(100);
        }

        public DataTable ReadData()
        {
            DataTable personTable = null;
            connector.Open();
            try
            {
                DbDataReader reader = connector.GetSelectAllCommand().ExecuteReader();
                personTable = new DataTable();
                personTable.Load(reader);
            }
            catch(SQLiteException ex)
            {
                MessageBox.Show(
                    string.Format("SQL error occurred!{0}{1}", Environment.NewLine, ex.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
            }

            connector.Close();

            return personTable;
        }

        private void LoadFile(string fileName, char valueSeparator, DbCommand command, Action<int> progressReporter)
        {
            using (StreamReader reader = new StreamReader(fileName))
            {
                int lineCount = 0;

                while (!reader.EndOfStream)
                {
                    lineCount++;
                    string[] values = reader.ReadLine().Split(valueSeparator);

                    if (values.Length == 3)
                    {
                        command.Parameters["@first_name"].Value = values[0];
                        command.Parameters["@last_name"].Value = values[1];
                        SetIntFied(fileName, lineCount, command, "salary", values[2]);

                        command.ExecuteNonQuery();
                    }

                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    //Put some delay into processing
                    Thread.Sleep(200);

                    progressReporter(10 + lineCount);
                }
            }
        }

        #region Value converters

        private void SetIntFied(string fileName, int lineCount, DbCommand command, string fieldName, string value)
        {
            try
            {
                command.Parameters["@" + fieldName].Value = int.Parse(value);
            }
            catch (Exception ex)
            {
                throw new FileParseException(fileName, lineCount, fieldName, value, "File parse error", ex);
            }
        }

        #endregion
    }
}
