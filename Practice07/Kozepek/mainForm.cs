﻿using System;
using System.Windows.Forms;

namespace Kozepek
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        private void Value_TextChanged(object sender, EventArgs e)
        {
            Compute.Enabled = X.Text.Length > 0 && Y.Text.Length > 0;
        }

        private void Compute_Click(object sender, EventArgs e)
        {
            try
            {
                double x = double.Parse(X.Text);
                double y = double.Parse(Y.Text);

                if (x == 0 || y == 0)
                {
                    MessageBox.Show("Egyik operandus sem lehet 0", "Figyelem!");
                }
                else
                {
                    Avg1.Text = String.Format(
                        "{0:0.0000}",
                        (x + y) / 2
                    );
                    Avg2.Text = String.Format(
                        "{0:0.0000}", 
                        Math.Sqrt(x * y)
                    );
                    Avg3.Text = String.Format(
                        "{0:0.0000}", 
                        2.0 / ((1.0 / x) + (1.0 / y))
                    );
                }
            }
            catch(FormatException fe)
            {
                MessageBox.Show(fe.Message, "Formátum hiba!");
            }
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            X.Clear();
            Y.Clear();
            Avg1.Clear();
            Avg2.Clear();
            Avg3.Clear();
        }

        private void Finish_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
