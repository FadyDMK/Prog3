﻿namespace Uszoverseny {
    partial class VersenyForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnKovetkezo = new System.Windows.Forms.Button();
            this.txtVersenyzo = new System.Windows.Forms.TextBox();
            this.btnVerseny = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tav = new System.Windows.Forms.NumericUpDown();
            this.versenyszam = new System.Windows.Forms.ComboBox();
            this.ido = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.tav)).BeginInit();
            this.SuspendLayout();
            // 
            // btnKovetkezo
            // 
            this.btnKovetkezo.Enabled = false;
            this.btnKovetkezo.Location = new System.Drawing.Point(544, 206);
            this.btnKovetkezo.Margin = new System.Windows.Forms.Padding(4);
            this.btnKovetkezo.Name = "btnKovetkezo";
            this.btnKovetkezo.Size = new System.Drawing.Size(100, 28);
            this.btnKovetkezo.TabIndex = 19;
            this.btnKovetkezo.Text = "Következő";
            this.btnKovetkezo.UseVisualStyleBackColor = true;
            this.btnKovetkezo.Click += new System.EventHandler(this.btnKovetkezo_Click);
            // 
            // txtVersenyzo
            // 
            this.txtVersenyzo.Location = new System.Drawing.Point(129, 127);
            this.txtVersenyzo.Margin = new System.Windows.Forms.Padding(4);
            this.txtVersenyzo.Name = "txtVersenyzo";
            this.txtVersenyzo.ReadOnly = true;
            this.txtVersenyzo.Size = new System.Drawing.Size(357, 22);
            this.txtVersenyzo.TabIndex = 17;
            // 
            // btnVerseny
            // 
            this.btnVerseny.Location = new System.Drawing.Point(544, 39);
            this.btnVerseny.Margin = new System.Windows.Forms.Padding(4);
            this.btnVerseny.Name = "btnVerseny";
            this.btnVerseny.Size = new System.Drawing.Size(100, 28);
            this.btnVerseny.TabIndex = 16;
            this.btnVerseny.Text = "Verseny";
            this.btnVerseny.UseVisualStyleBackColor = true;
            this.btnVerseny.Click += new System.EventHandler(this.btnVerseny_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(265, 45);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "m";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 212);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "Idő:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 130);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Versenyző:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 45);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Táv:";
            // 
            // tav
            // 
            this.tav.Location = new System.Drawing.Point(129, 42);
            this.tav.Maximum = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            this.tav.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.tav.Name = "tav";
            this.tav.Size = new System.Drawing.Size(120, 22);
            this.tav.TabIndex = 20;
            this.tav.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // versenyszam
            // 
            this.versenyszam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.versenyszam.FormattingEnabled = true;
            this.versenyszam.Items.AddRange(new object[] {
            "mell",
            "gyors",
            "hát",
            "pille",
            "vegyes"});
            this.versenyszam.Location = new System.Drawing.Point(304, 41);
            this.versenyszam.Name = "versenyszam";
            this.versenyszam.Size = new System.Drawing.Size(182, 24);
            this.versenyszam.TabIndex = 21;
            // 
            // ido
            // 
            this.ido.Enabled = false;
            this.ido.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.ido.Location = new System.Drawing.Point(129, 209);
            this.ido.Name = "ido";
            this.ido.ShowUpDown = true;
            this.ido.Size = new System.Drawing.Size(200, 22);
            this.ido.TabIndex = 22;
            // 
            // VersenyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(685, 273);
            this.Controls.Add(this.ido);
            this.Controls.Add(this.versenyszam);
            this.Controls.Add(this.tav);
            this.Controls.Add(this.btnKovetkezo);
            this.Controls.Add(this.txtVersenyzo);
            this.Controls.Add(this.btnVerseny);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VersenyForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "VersenyForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VersenyForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.tav)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnKovetkezo;
        private System.Windows.Forms.TextBox txtVersenyzo;
        private System.Windows.Forms.Button btnVerseny;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown tav;
        private System.Windows.Forms.ComboBox versenyszam;
        private System.Windows.Forms.DateTimePicker ido;
    }
}