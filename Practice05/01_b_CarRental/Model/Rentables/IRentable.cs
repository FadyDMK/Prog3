﻿namespace CarRent
{
    interface IRentable
    {
        int Fare { get; }
        bool IsAvailable { get; }
        bool Rent();
        bool Return();
    }
}
