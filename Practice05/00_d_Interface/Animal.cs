﻿namespace _00_d_Interface
{
    internal abstract class Animal : IAnimal
    {
        public abstract void Eat();
    }
}
