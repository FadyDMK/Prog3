﻿namespace Database
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.useMSSql = new System.Windows.Forms.RadioButton();
            this.useSQLite = new System.Windows.Forms.RadioButton();
            this.sqliteConnectionString = new System.Windows.Forms.TextBox();
            this.msSqlConnctionString = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.testConnection = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.fileName = new System.Windows.Forms.TextBox();
            this.browseFile = new System.Windows.Forms.Button();
            this.createTable = new System.Windows.Forms.Button();
            this.insertFile = new System.Windows.Forms.Button();
            this.readTable = new System.Windows.Forms.Button();
            this.dataList = new System.Windows.Forms.ListBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.name = new System.Windows.Forms.TextBox();
            this.salary = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // useMSSql
            // 
            this.useMSSql.AutoSize = true;
            this.useMSSql.Location = new System.Drawing.Point(9, 26);
            this.useMSSql.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.useMSSql.Name = "useMSSql";
            this.useMSSql.Size = new System.Drawing.Size(133, 20);
            this.useMSSql.TabIndex = 0;
            this.useMSSql.Text = "Microsoft SQL Server";
            this.useMSSql.UseVisualStyleBackColor = true;
            this.useMSSql.CheckedChanged += new System.EventHandler(this.connectionType_CheckedChanged);
            // 
            // useSQLite
            // 
            this.useSQLite.AutoSize = true;
            this.useSQLite.Location = new System.Drawing.Point(9, 49);
            this.useSQLite.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.useSQLite.Name = "useSQLite";
            this.useSQLite.Size = new System.Drawing.Size(111, 20);
            this.useSQLite.TabIndex = 1;
            this.useSQLite.Text = "SQLite database";
            this.useSQLite.UseVisualStyleBackColor = true;
            this.useSQLite.CheckedChanged += new System.EventHandler(this.connectionType_CheckedChanged);
            // 
            // sqliteConnectionString
            // 
            this.sqliteConnectionString.Enabled = false;
            this.sqliteConnectionString.Location = new System.Drawing.Point(136, 48);
            this.sqliteConnectionString.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.sqliteConnectionString.Name = "sqliteConnectionString";
            this.sqliteConnectionString.Size = new System.Drawing.Size(438, 20);
            this.sqliteConnectionString.TabIndex = 2;
            this.sqliteConnectionString.Text = "URI=file:sqlite.db";
            // 
            // msSqlConnctionString
            // 
            this.msSqlConnctionString.Enabled = false;
            this.msSqlConnctionString.Location = new System.Drawing.Point(136, 25);
            this.msSqlConnctionString.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.msSqlConnctionString.Name = "msSqlConnctionString";
            this.msSqlConnctionString.Size = new System.Drawing.Size(438, 20);
            this.msSqlConnctionString.TabIndex = 3;
            this.msSqlConnctionString.Text = "Data Source=server; Initial Catalog=database; User Id=userName; Password=password" +
    "";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(136, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(437, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "ConnectionString";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testConnection
            // 
            this.testConnection.Enabled = false;
            this.testConnection.Location = new System.Drawing.Point(240, 71);
            this.testConnection.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.testConnection.Name = "testConnection";
            this.testConnection.Size = new System.Drawing.Size(110, 24);
            this.testConnection.TabIndex = 5;
            this.testConnection.Text = "Test connection";
            this.testConnection.UseVisualStyleBackColor = true;
            this.testConnection.Click += new System.EventHandler(this.testConnection_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 125);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "File name:";
            // 
            // fileName
            // 
            this.fileName.Location = new System.Drawing.Point(136, 123);
            this.fileName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.fileName.Name = "fileName";
            this.fileName.Size = new System.Drawing.Size(413, 20);
            this.fileName.TabIndex = 7;
            // 
            // browseFile
            // 
            this.browseFile.Location = new System.Drawing.Point(549, 122);
            this.browseFile.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.browseFile.Name = "browseFile";
            this.browseFile.Size = new System.Drawing.Size(25, 22);
            this.browseFile.TabIndex = 8;
            this.browseFile.Text = "...";
            this.browseFile.UseVisualStyleBackColor = true;
            this.browseFile.Click += new System.EventHandler(this.browseFile_Click);
            // 
            // createTable
            // 
            this.createTable.Location = new System.Drawing.Point(17, 156);
            this.createTable.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.createTable.Name = "createTable";
            this.createTable.Size = new System.Drawing.Size(125, 37);
            this.createTable.TabIndex = 9;
            this.createTable.Text = "Create table";
            this.createTable.UseVisualStyleBackColor = true;
            this.createTable.Click += new System.EventHandler(this.createTable_Click);
            // 
            // insertFile
            // 
            this.insertFile.Location = new System.Drawing.Point(232, 156);
            this.insertFile.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.insertFile.Name = "insertFile";
            this.insertFile.Size = new System.Drawing.Size(125, 37);
            this.insertFile.TabIndex = 10;
            this.insertFile.Text = "Insert file content";
            this.insertFile.UseVisualStyleBackColor = true;
            this.insertFile.Click += new System.EventHandler(this.insertFile_Click);
            // 
            // readTable
            // 
            this.readTable.Location = new System.Drawing.Point(448, 156);
            this.readTable.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.readTable.Name = "readTable";
            this.readTable.Size = new System.Drawing.Size(125, 37);
            this.readTable.TabIndex = 11;
            this.readTable.Text = "Read table content";
            this.readTable.UseVisualStyleBackColor = true;
            this.readTable.Click += new System.EventHandler(this.readTable_Click);
            // 
            // dataList
            // 
            this.dataList.FormattingEnabled = true;
            this.dataList.Location = new System.Drawing.Point(17, 197);
            this.dataList.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataList.Name = "dataList";
            this.dataList.Size = new System.Drawing.Size(174, 160);
            this.dataList.TabIndex = 12;
            this.dataList.SelectedIndexChanged += new System.EventHandler(this.dataList_SelectedIndexChanged);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToOrderColumns = true;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView.Location = new System.Drawing.Point(232, 197);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowHeadersWidth = 62;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(340, 249);
            this.dataGridView.TabIndex = 13;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.progressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 437);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 10, 0);
            this.statusStrip1.Size = new System.Drawing.Size(584, 32);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(64, 25);
            this.statusLabel.Text = "Ready.";
            // 
            // progressBar
            // 
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(75, 24);
            this.progressBar.Visible = false;
            // 
            // name
            // 
            this.name.Enabled = false;
            this.name.Location = new System.Drawing.Point(17, 379);
            this.name.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(174, 20);
            this.name.TabIndex = 15;
            // 
            // salary
            // 
            this.salary.Enabled = false;
            this.salary.Location = new System.Drawing.Point(17, 418);
            this.salary.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.salary.Name = "salary";
            this.salary.Size = new System.Drawing.Size(174, 20);
            this.salary.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 362);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 401);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Salary:";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 469);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.salary);
            this.Controls.Add(this.name);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.dataList);
            this.Controls.Add(this.readTable);
            this.Controls.Add(this.insertFile);
            this.Controls.Add(this.createTable);
            this.Controls.Add(this.browseFile);
            this.Controls.Add(this.fileName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.testConnection);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.msSqlConnctionString);
            this.Controls.Add(this.sqliteConnectionString);
            this.Controls.Add(this.useSQLite);
            this.Controls.Add(this.useMSSql);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Database connection";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton useMSSql;
        private System.Windows.Forms.RadioButton useSQLite;
        private System.Windows.Forms.TextBox sqliteConnectionString;
        private System.Windows.Forms.TextBox msSqlConnctionString;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button testConnection;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox fileName;
        private System.Windows.Forms.Button browseFile;
        private System.Windows.Forms.Button createTable;
        private System.Windows.Forms.Button insertFile;
        private System.Windows.Forms.Button readTable;
        private System.Windows.Forms.ListBox dataList;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox salary;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

