﻿using System;
using System.Windows.Forms;

namespace TextWindowsForms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void greetings_Click(object sender, EventArgs e)
        {
            if (lastName.Text.Length > 0
                && firstName.Text.Length > 0
                && phoneNumber.MaskCompleted)
            {
                message.Text = string.Format("{0} {1}", lastName.Text, firstName.Text)
                    + Environment.NewLine
                    + phoneNumber.Text;
            }
            else
            {
                MessageBox.Show(
                    "Kérem töltse ki a vezetéknév, keresztnév, telefonszám mezőket!",
                    "Figyelem"
                );
            }
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
