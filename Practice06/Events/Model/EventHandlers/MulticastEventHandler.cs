﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    class MulticastEventHandler : IKeyEventHandler
    {
        public delegate bool KeyPressHandler(char pressed);

        public KeyPressHandler eventHandlers { get; set; }

        public bool HandleKeyEvent(char eventKey)
        {
            //This call would call all referenced methods automatically, 
            // but return the value returned by the last method call, all others are absorbed
            //return eventHandlers(eventKey);

            bool returnValue = true;

            //If combination of returned values is required, separated calls must be placed
            //Items of references to call is available via: GetInvocationList() method
            foreach (KeyPressHandler methodReference in eventHandlers.GetInvocationList())
            {
                returnValue &= methodReference(eventKey);
            }

            return returnValue;
        }
    }
}
