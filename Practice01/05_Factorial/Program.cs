﻿using System;

namespace _05_Factorial
{
    class Program
    {
        private const int MAX_N = 20;

        static void Main(string[] args)
        {
            //Get a valid integer from the console to make the application readable
            int n = GetIntFromConsole();

            //Calculating factorial in a method to make the code readable
            Console.WriteLine("{0}! = {1}", n, CalcFactorial(n));
        }

        private static int GetIntFromConsole()
        {
            int n;

            Console.Write("Enter an integer number (1-{0}) ", MAX_N);

            //Reading from console and value checking in one line.
            //Valid, because n is initialized by TryParse to check, otherwise logical expression fails and n is not checked
            while (!int.TryParse(Console.ReadLine(), out n) || n < 0 || n > MAX_N)
            {
                Console.Write("Please try again: ");
            }

            return n;
        }

        private static int CalcFactorial(int n)
        {
            int result = 1;
            for (int i = 1; i <= n; i++)
            {
                result *= i;
            }

            return result;
        }
    }
}
