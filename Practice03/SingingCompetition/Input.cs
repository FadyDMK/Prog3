﻿using System;

namespace SingingCompetition
{
    class Input
    {
        public static void Pause()
        {
            Console.Write("Nyomjon le egy billentyűt! ");
            Console.ReadKey();
            Console.WriteLine();
        }

        public static int GetNumerFromConsole(string message)
        {
            int number;

            Console.Write(message);
            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.Write(message);
            }

            return number;
        }
    }
}
