﻿using System.Collections.Generic;

namespace Workers
{
    //Interface to define text parser functionality
    //Implementations of this interface will be responsible for parsing specific text structures
    //
    //It is not an abstract class, to make these requirements independent from class hierarchy
    //
    //Type parameter lets the interface be used in a strongly typed mode for all classes
    interface ITextParser<T>
    {
        List<T> ParseListFromMultilineText(string multilineText);
    }
}
