﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uszoverseny
{
    public class Versenyzo : IComparable
    {
        public string Nev { get; private set; }
        public string Orszag { get; private set; }
        public string Zaszlo { get; private set; }
        public string RajtSzam { get; private set; }
        public TimeSpan IdoEredmeny { get; private set; }

        private static int sorszam;

        public Versenyzo(string nev, string orszag, string zaszlo)
        {
            this.Nev = nev;
            this.Orszag = orszag;
            this.Zaszlo = zaszlo;
            sorszam++;
            this.RajtSzam = (sorszam < 10) ? ("0" + sorszam) : sorszam.ToString();
        }

        public void Versenyez(TimeSpan idoEredmeny)
        {
            this.IdoEredmeny = idoEredmeny;
        }

        public override string ToString()
        {
            return Nev + " " + IdoEredmeny.ToString();
        }

        public string GetContentString()
        {
            return RajtSzam + ";" + Nev + ";" + Orszag + ";" + IdoEredmeny.ToString();
        }

        public int CompareTo(object obj)
        {
            Versenyzo other = (Versenyzo)obj;

            return IdoEredmeny.CompareTo(other.IdoEredmeny);
        }
    }
}
