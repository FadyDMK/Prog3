﻿namespace Retoxi.View.Controls
{
    partial class DrinkControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.order = new System.Windows.Forms.CheckBox();
            this.count = new System.Windows.Forms.TextBox();
            this.portion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // order
            // 
            this.order.AutoSize = true;
            this.order.Location = new System.Drawing.Point(13, 11);
            this.order.Name = "order";
            this.order.Size = new System.Drawing.Size(113, 24);
            this.order.TabIndex = 0;
            this.order.Text = "checkBox1";
            this.order.UseVisualStyleBackColor = true;
            // 
            // count
            // 
            this.count.Location = new System.Drawing.Point(474, 10);
            this.count.Name = "count";
            this.count.Size = new System.Drawing.Size(100, 26);
            this.count.TabIndex = 1;
            // 
            // portion
            // 
            this.portion.AutoSize = true;
            this.portion.Location = new System.Drawing.Point(580, 13);
            this.portion.Name = "portion";
            this.portion.Size = new System.Drawing.Size(51, 20);
            this.portion.TabIndex = 2;
            this.portion.Text = "label1";
            // 
            // DrinkControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.portion);
            this.Controls.Add(this.count);
            this.Controls.Add(this.order);
            this.Name = "DrinkControl";
            this.Size = new System.Drawing.Size(681, 46);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox order;
        private System.Windows.Forms.TextBox count;
        private System.Windows.Forms.Label portion;
    }
}
