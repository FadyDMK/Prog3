﻿using System;
using System.Speech.Synthesis;

namespace TextToSpeech
{
    class Voice
    {
        public VoiceInfo Details { get; set; }
        public string Id { get { return Details.Name; } }

        public Voice(VoiceInfo details)
        {
            Details = details;
        }

        public override string ToString()
        {
            return $"Id: {Details.Id} | Name: {Details.Name} | Age: { Details.Age} | Gender: { Details.Gender} | Culture: { Details.Culture}";
        }
    }
}
