﻿using System;

namespace Database
{
    class FileParseException : Exception
    {
        public string FileName { get; private set; }
        public int LineNumber { get; private set; }
        public string FieldName { get; private set; }
        public string Value { get; private set; }

        public FileParseException(string fileName, int lineNumber, string fieldName, string value, string message, Exception innerException) : base(message, innerException)
        {
            FileName = fileName;
            LineNumber = lineNumber;
            FieldName = fieldName;
            Value = value;
        }

        public override string ToString()
        {
            return string.Format("{0} ({1} of field: {2} [{3}] in line {4}", Message, InnerException.Message, FieldName, Value, LineNumber);
        }
    }
}
