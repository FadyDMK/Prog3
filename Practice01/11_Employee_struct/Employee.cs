﻿using System;

namespace _11_Employee_struct
{
    struct Employee
    {
        public string Name;
        public int WorkedHours;
        public int HourlyWage;

        public int CalculateWage()
        {
            return WorkedHours * HourlyWage;
        }

        public override string ToString()
        {
            return String.Format("{0} wage: {1}HUF", Name, CalculateWage());
        }
    }
}
