﻿namespace _99_Logger
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = new Logger("Logger.log");

            logger.Write("First message", LogLevels.Error);
            logger.Write("Second message", LogLevels.Verbose);
            logger.Write("Third message", LogLevels.Fatal);
            logger.Write("Fourth message", LogLevels.Info);

            MultiChannelLogger mcl = new MultiChannelLogger();

            mcl.AddChannel(new FileChannel("FileChannel.log"));
            mcl.AddChannel(new ConsoleChannel());

            mcl.Write("Fifth message", LogLevels.Error);
            mcl.Write("Sixth message", LogLevels.Verbose);
            mcl.Write("Seventh message", LogLevels.Fatal);
            mcl.Write("Eighth message", LogLevels.Info);

            DecoratedMultiChannelLogger dmcl = new DecoratedMultiChannelLogger(
                (message, level) => string.Format("[{1}] {0}", message, level),
                new DateDecorator(new TimeDecorator())
            );

            dmcl.AddChannel(new FileChannel("DecoratedFileChannel.log"));
            dmcl.AddChannel(new ConsoleChannel());

            dmcl.Write("Ninth message", LogLevels.Error);
            dmcl.Write("Tenth message", LogLevels.Verbose);
            dmcl.Write("Eleventh message", LogLevels.Fatal);
            dmcl.Write("Twelfth message", LogLevels.Info);
        }
    }
}
