﻿using System;
using System.Windows.Forms;
using System.Speech.Synthesis;

namespace TextToSpeech
{
    public partial class mainForm : Form
    {
        private SpeechSynthesizer synthesizer = null;
        private bool speaking = false;

        public mainForm()
        {
            InitializeComponent();

            synthesizer = new SpeechSynthesizer();
            synthesizer.SpeakCompleted += Synthesizer_SpeakCompleted;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (SpeechSynthesizer synthesizer = new SpeechSynthesizer())
            {
                foreach (var voice in synthesizer.GetInstalledVoices())
                {
                    var info = voice.VoiceInfo;
                    availableVoices.Items.Add(new Voice(info));
                }

                availableVoices.SelectedIndex = 0;
            }
        }

        private void availableVoices_SelectedIndexChanged(object sender, EventArgs e)
        {
            speak.Enabled = availableVoices.SelectedIndex >= 0 && !speaking;
        }

        private void speak_Click(object sender, EventArgs e)
        {
            //Used in Async mode
            speaking = true;
            speak.Enabled = false;

            synthesizer.Volume = volume.Value;
            synthesizer.SelectVoice(((Voice)availableVoices.SelectedItem).Id);
            synthesizer.SetOutputToDefaultAudioDevice();

            //synthesizer.Speak(toSpeak.Text);
            synthesizer.SpeakAsync(toSpeak.Text);
        }

        private void Synthesizer_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
        {
            //Used in Async mode
            speak.Enabled = true;
            speaking = false;
        }

    }
}
