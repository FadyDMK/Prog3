﻿namespace _00_HF2
{
    class Record : Publication
    {
        //Specialized class property (automatic)
        public static int AdditionalPrice { get; set; }

        public string Composer { get; private set; }
        public int NumberOfTracks { get; private set; }
        public int Length { get; private set; }

        //Specialized computed instance property to utilize new price component
        public override int Price { get { return base.Price + AdditionalPrice; } }

        public Record(string title, string publisher, 
            string composer,
            int numberFoTracks, int length) : base(title, publisher)
        {
            Composer = composer;
            NumberOfTracks = numberFoTracks;
            Length = length;
        }

        //Specialized price calculation
        //uses price calculation of base class
        //Overriding of this method is not required, because base implementation
        //uses overrode version of instance property, therefore base Price behaviour has been changed
        public override int RentalPrice(int days)
        {
            return base.RentalPrice(days) + AdditionalPrice * days;
        }

        public override string ToString()
        {
            return string.Format("{0}: {1} ({2}) [{3}/day]",
                Composer, Title, Publisher, Price);
        }
    }
}
