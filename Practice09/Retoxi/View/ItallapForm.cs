﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Retoxi
{
    public partial class ItallapForm : Form
    {
        private List<Ital> italok;
        private List<CheckBox> nevek = new List<CheckBox>();
        private List<TextBox> darabok = new List<TextBox>();

        public ItallapForm(List<Ital> italok)
        {
            InitializeComponent();
            this.italok = italok;
        }

        private const int bal = 10, fent = 10;
        private const int chkXMeret = 250, chkYMeret = 21;
        private const int chkYTav = 40;
        private const int txtXMeret = 30, txtYMeret = 17;
        private const int lblXMeret = 46;

        private const int xKoz = 5;

        private const int maxAdag = 999;

        private void ItallapForm_Load(object sender, EventArgs e)
        {
            CreateDynamicFormContent();
        }

        private void CreateDynamicFormContent()
        {
            for(int i=0; i<italok.Count; i++)
            {
                CreateControlsOfItem(italok[i], i);
            }
        }

        private void CreateControlsOfItem(Ital item, int index)
        {
            CheckBox nev;
            TextBox db;
            Label adag;

            nev = new CheckBox();
            nev.Text = item.Arlistaba();
            nev.Location = new Point(bal, fent + index * chkYTav);
            nev.Size = new Size(chkXMeret, chkYMeret);

            db = new TextBox();
            db.Location = new Point(
                bal + nev.Size.Width + xKoz,
                fent + index * chkYTav
            );
            db.MaxLength = 3;
            db.Size = new Size(txtXMeret, txtYMeret);

            adag = new Label();
            adag.Text = "adag";
            adag.Location = new Point(
                db.Location.X + db.Size.Width + xKoz,
                fent + index * chkYTav
            );
            adag.Size = new Size(lblXMeret, txtYMeret);

            ContentContainer.Controls.Add(nev);
            ContentContainer.Controls.Add(db);
            ContentContainer.Controls.Add(adag);

            nevek.Add(nev);
            darabok.Add(db);
        }

        private void Order_Click(object sender, EventArgs e)
        {
            bool hasError = false;
            for (int i = 0; i < italok.Count; i++)
            {
                darabok[i].BackColor = Color.White;

                if (nevek[i].Checked)
                {
                    int db = 0;
                    bool isNumber = int.TryParse(darabok[i].Text, out db);

                    if (!isNumber || db < 1 || db > maxAdag)
                    {
                        darabok[i].BackColor = Color.Salmon;
                        hasError = true;
                    }
                    else
                    {
                        italok[i].Rendeles += db;
                        nevek[i].Checked = false;
                        darabok[i].Clear();
                    }
                }
                else
                {
                    darabok[i].Clear();
                }
            }

            if (hasError)
            {
                MessageBox.Show(
                    "A pirossal jelzett adatok hibásak!",
                    "Figyelem!"
                );
            }
        }

        private void billToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OrdersForm ordersForm = new OrdersForm(italok);
            ordersForm.ShowDialog();
        }

        private void payToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach(Ital ital in italok)
            {
                ital.FizetettRendeles += ital.Rendeles;
                ital.Rendeles = 0;
            }

            MessageBox.Show("A számla fizetve!");
        }
    }
}
