﻿using System;

namespace _99_Logger
{
    class ConsoleChannel : ILogChannel
    {
        public void Write(string message)
        {
            Console.WriteLine(message);
        }
    }
}
