﻿using System;

namespace _00_e_enum
{
    internal class Program
    {
        enum Volume : byte { Silent=1, Normal=2, Loud=3 };
        static void Main(string[] args)
        {
            Volume music = Volume.Silent;
            Console.WriteLine(music);
            Console.WriteLine((int)music);
            music++;
            Console.WriteLine(music);
            Console.WriteLine((int)music);
            music++;
            Console.WriteLine(music);
            Console.WriteLine((int)music);
            music++;
            Console.WriteLine(music);
            Console.WriteLine((int)music);

            string strMusic = Volume.Silent.ToString();
            Console.WriteLine(strMusic);

            Volume convertedMusic = (Volume)Enum.Parse(
                typeof(Volume), 
                strMusic
                );
            Console.WriteLine(convertedMusic);
        }
    }
}
