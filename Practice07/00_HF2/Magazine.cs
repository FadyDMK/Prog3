﻿namespace _00_HF2
{
    class Magazine : Publication
    {
        //Specialized class property (automatic)
        public static int AdditionalPrice { get; set; }

        //Specialized instance properties (automatic)
        public int Year { get; private set; }
        public int Number { get; private set; }

        //Specialized computed instance property to utilize new price component
        public override int Price { get { return base.Price + AdditionalPrice; } }

        public Magazine(string title, string publisher,
            int year, int number
            ) : base(title, publisher)
        {
            Year = year;
            Number = number;
        }

        //Specialized price calculation
        //uses price calculation of base class
        //Overriding of this method is not required, because base implementation
        //uses overrode version of instance property, therefore base Price behaviour has been changed
        public override int RentalPrice(int days)
        {
            return base.RentalPrice(days) + AdditionalPrice * days;
        }

        public override string ToString()
        {
            return string.Format("({0}) {1} {2}/{3} [{4}/day]",
                Publisher, Title, Year, Number, Price);
        }
    }
}
