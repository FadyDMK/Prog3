﻿using System;

namespace RLE
{
    class Program
    {
        static Random random = new Random();

        static void Main(string[] args)
        {
            string[] parts = new string[] { "A", "B", "C", "D"};

            string originalContent = "CCCCCDDDAAAAAAAAAABBBBDCCCBBBDDDDDDCCCCCCCCDDDDDAAADDDAAAAAABBDDCABBDDBBB";
            string marker = "#";

            Console.WriteLine(String.Format("Original: [{0}] {1}", originalContent, originalContent.Length));

            string encodedWithMarker = EncodeWithMarker(originalContent, marker);
            Console.WriteLine(String.Format("Encoded with marker: [{0}] {1}", encodedWithMarker.Length, encodedWithMarker));

            Console.WriteLine(String.Format("Decoded with marker: {0}", DecodeWithMarker(encodedWithMarker, marker)));

            string encodedWithoutMarker = EncodeWithoutMarker(originalContent);
            Console.WriteLine(String.Format("Encoded without marker: [{0}] {1}", encodedWithoutMarker.Length, encodedWithoutMarker));

            Console.WriteLine(String.Format("Decoded without marker: {0}", DecodeWithoutMarker(encodedWithoutMarker)));

            string rawText;
            string encodedText;

            for (int i = 0; i < 20; i++)
            {
                rawText = RandomString(parts, random.Next(20, 40));
                encodedText = EncodeWithMarker(rawText, marker);
                Console.WriteLine("{0}-{1}-{2}",
                    rawText,
                    encodedText,
                    DecodeWithMarker(encodedText, marker)
                );
            }
        }

        static string RandomString(string[] parts, int len)
        {
            string result = "";
            for (int i = 0; i < len; i++)
            {
                result += parts[random.Next(parts.Length)];
            }

            return result;
        }

        static string EncodeWithMarker(string original, string marker)
        {
            return "";
        }

        static string DecodeWithMarker(string encoded, string marker)
        {
            return "";
        }

        static string EncodeWithoutMarker(string original)
        {
            return "";
        }

        static string DecodeWithoutMarker(string encoded)
        {
            return "";
        }
    }
}
