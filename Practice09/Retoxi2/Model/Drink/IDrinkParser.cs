﻿using Retoxi.Model;
using System.Collections.Generic;

namespace Retoxi
{
    public interface IDrinkParser : IParser<Drink>
    {
        List<Drink> Parse();
    }
}
