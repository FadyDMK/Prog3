﻿using System.Data;
using System.Data.Common;
using System.Data.SQLite;

//Right mouse click on project in Solution Explorer
//Select Manage NuGet Packages...
//Browse for System.Data.Sqlite package and install it with prerequisites

namespace Database
{
    class SQLiteConnector : IDbConnector
    {
        const string CREATE_TABLE = "CREATE TABLE \"person\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \"first_name\" TEXT NOT NULL, \"last_name\" TEXT NOT NULL, \"salary\" INTEGER NOT NULL)";
        const string INSERT_TABLE_COMMAND = "INSERT INTO person (first_name, last_name, salary) values (@first_name, @last_name, @salary)";
        const string SELECT_TABLE_COMMAND = "SELECT * FROM person";

        SQLiteConnection connection;

        public SQLiteConnector(string connectionString)
        {
            connection = new SQLiteConnection(connectionString);
        }

        public void Open()
        {
            connection.Open();
        }

        public void Close()
        {
            connection.Close();
        }

        public DbCommand GetCreateCommand()
        {
            SQLiteCommand command = new SQLiteCommand(CREATE_TABLE, connection);

            return command;
        }

        public DbCommand GetInsertCommand()
        {
            SQLiteCommand command = new SQLiteCommand(
                INSERT_TABLE_COMMAND,
                connection
            );

            command.Parameters.Add(new SQLiteParameter("@first_name", DbType.String));
            command.Parameters.Add(new SQLiteParameter("@last_name", DbType.String));
            command.Parameters.Add(new SQLiteParameter("@salary", DbType.Int32));

            command.Prepare();

            return command;
        }

        public DbCommand GetSelectAllCommand()
        {
            SQLiteCommand command = new SQLiteCommand(
                SELECT_TABLE_COMMAND,
                connection
            );

            return command;
        }
    }
}
