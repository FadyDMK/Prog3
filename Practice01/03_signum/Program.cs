﻿using System;

namespace _03_signum
{
    class Program
    {
        static void Main(string[] args)
        {
            string data;
            int number;

            //Get a string from the console until it can be converted to an integer value
            do
            {
                Console.Write("Enter an integer number: ");
                data = Console.ReadLine();
            } while (!int.TryParse(data, out number));

            //Check signumf of the entered integer
            if (number == 0)
            {
                Console.WriteLine("The number is 0");
            }
            else
            {
                if(number < 0)
                {
                    Console.WriteLine("The number is negative");
                }
                else
                {
                    Console.WriteLine("The number is positive");
                }
            }
        }
    }
}
