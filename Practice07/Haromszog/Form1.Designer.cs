﻿namespace Haromszog
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.A = new System.Windows.Forms.TextBox();
            this.B = new System.Windows.Forms.TextBox();
            this.C = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Compute = new System.Windows.Forms.Button();
            this.Clear = new System.Windows.Forms.Button();
            this.Finish = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "a oldal";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(206, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "b oldal";
            // 
            // A
            // 
            this.A.Location = new System.Drawing.Point(17, 36);
            this.A.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.A.Name = "A";
            this.A.Size = new System.Drawing.Size(112, 26);
            this.A.TabIndex = 2;
            this.A.TextChanged += new System.EventHandler(this.Side_TextChanged);
            // 
            // B
            // 
            this.B.Location = new System.Drawing.Point(209, 36);
            this.B.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(112, 26);
            this.B.TabIndex = 3;
            this.B.TextChanged += new System.EventHandler(this.Side_TextChanged);
            // 
            // C
            // 
            this.C.Location = new System.Drawing.Point(209, 114);
            this.C.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.C.Name = "C";
            this.C.ReadOnly = true;
            this.C.Size = new System.Drawing.Size(112, 26);
            this.C.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(209, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "c oldal";
            // 
            // Compute
            // 
            this.Compute.Enabled = false;
            this.Compute.Location = new System.Drawing.Point(17, 112);
            this.Compute.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Compute.Name = "Compute";
            this.Compute.Size = new System.Drawing.Size(84, 29);
            this.Compute.TabIndex = 6;
            this.Compute.Text = "Számol";
            this.Compute.UseVisualStyleBackColor = true;
            this.Compute.Click += new System.EventHandler(this.Compute_Click);
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(17, 199);
            this.Clear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(84, 38);
            this.Clear.TabIndex = 7;
            this.Clear.Text = "Töröl";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Finish
            // 
            this.Finish.Location = new System.Drawing.Point(237, 199);
            this.Finish.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Finish.Name = "Finish";
            this.Finish.Size = new System.Drawing.Size(84, 38);
            this.Finish.TabIndex = 8;
            this.Finish.Text = "Vége";
            this.Finish.UseVisualStyleBackColor = true;
            this.Finish.Click += new System.EventHandler(this.Finish_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 255);
            this.Controls.Add(this.Finish);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.Compute);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.C);
            this.Controls.Add(this.B);
            this.Controls.Add(this.A);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Derékszögű háromszög";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox A;
        private System.Windows.Forms.TextBox B;
        private System.Windows.Forms.TextBox C;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Compute;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Button Finish;
    }
}

