﻿using System;

namespace _00_d_Interface
{
    internal class GermanShepherd : Animal, IDog
    {
        public override void Eat()
        {
            Console.WriteLine("Eating...");
        }

        public void Bark()
        {
            Console.WriteLine("Barking...");
        }
    }
}
