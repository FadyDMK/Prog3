﻿using System;

namespace _00_b_delegates
{
    class Printer
    {
        public static void StaticPrint()
        {
            Console.WriteLine("Printer from static base");
        }

        public string Mode { get; set; }

        public Printer(string mode)
        {
            this.Mode = mode;
        }

        //Overridable (virtual) instance method 
        public virtual void Print()
        {
            Console.WriteLine("Print from printer base in {0} mode", Mode);
        }
    }
}
