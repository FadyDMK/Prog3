﻿namespace Retoxi
{
    public class Drink
    {
        public string Name { get; private set; }
        public int UnitPrice { get; private set; }

        public int UnpayedOrder { get; set; }
        public int PayedOrder { get; set; }

        public int OrderPrice
        {
            get
            {
                return UnpayedOrder * UnitPrice;
            }
        }

        public Drink(string name, int unitPrice)
        {
            Name = name;
            UnitPrice = unitPrice;
            UnpayedOrder = 0;
            PayedOrder = 0;
        }

        public override string ToString()
        {
            return Name.PadRight(33) + ":" + UnitPrice.ToString().PadLeft(6) + " Ft";
        }

    }
}
