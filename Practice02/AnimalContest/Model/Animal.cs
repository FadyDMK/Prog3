﻿using System;

namespace AnimalContest.Model
{
    class Animal
    {
        //Private class state descriptors
        //  they apply to all instances
        static bool isInitialized = false;
        static int maxAge;
        static int currentYear;

        //Static initializer to initialize the class properties
        public static void Init(int maxAge, int currentYear)
        {
            Animal.maxAge = maxAge;
            Animal.currentYear = currentYear;
            isInitialized = true;
        }

        //Declaration of automatic properties
        public string Name { get; private set; }
        public int BirthYear { get; private set; }
        public int BeautyPoints { get; private set; }
        public int BehaviourPoints { get; private set; }

        public string VaccCertNum { get; private set; }

        public int Id { get; private set; }

        //Private data member (not accessible from outside of the class)
        private bool isEvaluated;

        //Computed property
        public int Age
        {
            get
            {
                return currentYear - BirthYear;
            }
        }

        //Computed property
        public int TotalPoints {
            get
            {
                if (Age > maxAge) return 0;

                return 
                    (maxAge - Age)*BeautyPoints
                    + Age * BehaviourPoints;
            }
        }

        //Ctor
        public Animal(string name, int birthYear, string vaccCertNum, int id)
        {
            if (!isInitialized)
                throw new Exception("Please Init class first!");

            Name = name;
            BirthYear = birthYear;
            VaccCertNum = vaccCertNum;
            Id = id;
            isEvaluated = false;
        }

        //Instance method
        public void Evaluation(int beautyPoint, int behaviourPoint)
        {
            BeautyPoints = beautyPoint;
            BehaviourPoints = behaviourPoint;
            isEvaluated = true;
        }

        //Override ToString to generate string representation
        public override string ToString()
        {
            if (!isEvaluated)
                return String.Format("{0} has not been evaluated", Name);

            return String.Format("{0} [{1}] score: {2}", 
                Name, Id, TotalPoints
                );
        }
    }
}
