﻿using System.Collections.Generic;
using System.IO;

namespace Retoxi.Model
{
    internal class Loader<T>
    {
        public List<T> LoadFile(string filePath, IParser<T> parser)
        {
            List<T> list = new List<T>();

            using (StreamReader reader = new StreamReader(filePath))
            {
                string[] dataItems;
                while (!reader.EndOfStream)
                {
                    dataItems = reader.ReadLine().Split(';');
                    list.Add(
                        parser.Parse(dataItems)
                    );
                }
            }

            return list;
        }
    }
}
