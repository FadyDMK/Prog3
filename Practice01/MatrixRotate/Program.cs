﻿using System;

namespace MatrixRotate
{
    internal class Program
    {
        static int rows, columns, shift;

        static int LayerOfNextPoint(int[] coord, int[,] indexDeltas)
        {
            int newY = coord[0] + indexDeltas[coord[2], 0];
            int newX = coord[1] + indexDeltas[coord[2], 1];

            return Math.Min(
                Math.Min(newX, columns - newX - 1),
                Math.Min(newY, rows - newY - 1)
                );
        }

        static void Shift(int[] coord, int[,] indexDeltas)
        {
            coord[0] += indexDeltas[coord[2], 0];
            coord[1] += indexDeltas[coord[2], 1];
        }

        static void GetNext(int[] coord, int layer, int[,] indexDeltas)
        {
            int layerOfNextPoint = LayerOfNextPoint(coord, indexDeltas);

            if(layerOfNextPoint < layer)
            {
                coord[2] = (coord[2] + 1) % 4;
            }

            Shift(coord, indexDeltas);
        }

        static void Main(string[] args)
        {
            int[,] indexDeltas = new int[,] {
                { 1,  0},
                { 0,  1},
                {-1,  0},
                { 0, -1}
            };

            Console.Write("Please enter row count: ");
            rows = int.Parse(Console.ReadLine());
            Console.Write("Please enter column count: ");
            columns = int.Parse(Console.ReadLine());
            Console.Write("Please enter shift: ");
            shift = int.Parse(Console.ReadLine());

            char[,] matrix = new char[rows, columns];

            string input;
            for (int r = 0; r < rows; r++)
            {
                Console.Write("Please enter {0}th row ({1} chars): ", r + 1, columns);
                input = Console.ReadLine();
                for(int c = 0; c < input.Length; c++)
                {
                    matrix[r, c] = input[c];
                }
            }

            int layers = Math.Min(rows, columns) / 2;
            int[] current, previous;
            char tempValue;
            for (int l = 0; l < layers; l++)
            {
                for (int s = 0; s < shift; s++)
                {
                    current = new int[] { l, l, 0 };
                    previous = new int[] { l + 1, l, 0 };

                    tempValue = matrix[current[0], current[1]];

                    do
                    {
                        matrix[current[0], current[1]] = matrix[previous[0], previous[1]];
                        GetNext(current, l, indexDeltas);
                        GetNext(previous, l, indexDeltas);
                    }
                    while (previous[0] != l || previous[1] != l);

                    matrix[current[0], current[1]] = tempValue;
                }
            }

            for (int y = 0; y < rows; y++)
            {
                for (int x = 0; x < columns; x++)
                {
                    Console.Write(matrix[y, x]);
                }
                Console.WriteLine();
            }
        }
    }
}