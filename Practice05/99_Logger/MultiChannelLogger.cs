﻿using System.Collections.Generic;

namespace _99_Logger
{
    class MultiChannelLogger
    {
        public LogLevels Level { get; set; }
        protected List<ILogChannel> channels;

        public MultiChannelLogger()
        {
            Level = LogLevels.Error;
            channels = new List<ILogChannel>();
        }

        public void AddChannel(ILogChannel newChannel)
        {
            channels.Add(newChannel);
        }

        public virtual void Write(string message, LogLevels messageLevel)
        {
            if (messageLevel <= Level)
            {
                foreach(ILogChannel channel in channels)
                {
                    channel.Write(message);
                }
            }
        }
    }
}
