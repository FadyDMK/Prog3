﻿using System;
using System.Diagnostics;
using System.Text;

namespace GitLoggerAsyncEvt
{
    class GitInfo
    {
        public enum CommandTypes
        {
            None,
            List,
            Details,
            Close,
            Custom
        }

        public const string RESPONSE_TERMINATOR = "GitResponseEnd";

        private Process cmdProcess;
        private StringBuilder receivedContents = new StringBuilder();
        private CommandTypes currentCommandType = CommandTypes.None;

        public string ReceivedString { get { return receivedContents.ToString(); } }

        public GitInfo()
        {
            cmdProcess = new Process();
            cmdProcess.StartInfo.FileName = "cmd.exe";
            cmdProcess.StartInfo.RedirectStandardInput = true;
            cmdProcess.StartInfo.RedirectStandardOutput = true;
            cmdProcess.StartInfo.StandardOutputEncoding = Encoding.UTF8;
            cmdProcess.StartInfo.CreateNoWindow = true;
            cmdProcess.StartInfo.UseShellExecute = false;

            cmdProcess.EnableRaisingEvents = true;
            cmdProcess.OutputDataReceived += Process_OutputDataReceived;
            cmdProcess.Exited += Process_Exited;

            cmdProcess.Start();

            cmdProcess.BeginOutputReadLine();
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                if (e.Data.Contains(RESPONSE_TERMINATOR))
                {
                    OnGitResponseReceived();
                    currentCommandType = CommandTypes.None;
                }
                else
                {
                    receivedContents.Append(e.Data);
                    receivedContents.Append(Environment.NewLine);
                }
            }
        }

        private void Process_Exited(object sender, EventArgs e)
        {
            receivedContents.Append("Process closed");
            receivedContents.Append(Environment.NewLine);

            OnGitInfoClosed();

            currentCommandType = CommandTypes.None;
        }

        public void ListCommits()
        {
            currentCommandType = CommandTypes.List;
            SendGitCommand("git log --shortstat");
        }

        public void CommitDetails(string commitId)
        {
            currentCommandType = CommandTypes.Details;
            SendGitCommand(string.Format("git diff-tree --no-commit-id --name-only -r {0}", commitId));
        }

        public void CloseProcess()
        {
            currentCommandType = CommandTypes.Close;
            SendTerminalCommand("exit");
        }

        private void SendGitCommand(string command)
        {
            receivedContents.Clear();
            SendTerminalCommand(command);
            SendTerminalCommand(String.Format("echo {0}", RESPONSE_TERMINATOR));
        }

        private void SendTerminalCommand(string command)
        {
            cmdProcess.StandardInput.WriteLine(command);
            cmdProcess.StandardInput.Flush();
        }

        protected virtual void OnGitResponseReceived()
        {
            EventHandler<GitResponseReceivedEventArgs> handler = GitResponseReceived;
            handler?.Invoke(this, new GitResponseReceivedEventArgs(currentCommandType, receivedContents.ToString()));

            //Equivalent with the previous line
            //if (handler != null)
            //{
            //    handler(this, new GitResponseReceivedEventArgs(receivedContents.ToString()));
            //}
        }

        protected virtual void OnGitInfoClosed()
        {
            EventHandler<EventArgs> handler = GitInfoClosed;
            handler?.Invoke(this, new EventArgs());
        }

        public event EventHandler<GitResponseReceivedEventArgs> GitResponseReceived;
        public event EventHandler<EventArgs> GitInfoClosed;
    }
}
