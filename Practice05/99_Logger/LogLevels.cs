﻿namespace _99_Logger
{
    public enum LogLevels
    {
        Fatal = 1,
        Error = 2,
        Warning = 3,
        Info = 4,
        Verbose = 5
    }
}
