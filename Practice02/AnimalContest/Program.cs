﻿using AnimalContest.Model;
using System;

namespace AnimalContest
{
    class Program
    {
        //Instance parameter related
        const int YEAR_MIN = 2000;
        const int YEAR_MAX = 2030;

        const int AGE_MIN = 10;
        const int AGE_MAX = 200;

        //Instance related, 
        const int POINT_MIN = 1;
        const int POINT_MAX = 10;

        //Create a pseudo-random sequence seeded by the current system clock
        static Random random = new Random();

        static void Main(string[] args)
        {
            //Read parameters from console
            Console.Write("Current year: ");
            int currYear = ConsoleInput.GetIntFromConsole(YEAR_MIN, YEAR_MAX);
            Console.Write("Contestant max age: ");
            int maxAge = ConsoleInput.GetIntFromConsole(AGE_MIN, AGE_MAX);

            //Get evaluation type selection
            bool randomEvaluation = ConsoleInput.GetKeyFromConsole("Do you want random evaluation? (y/n) ", 'y');

            //Create new instance of Contest
            Contest animalContest = new Contest(
                currYear, maxAge, randomEvaluation,
                POINT_MIN, POINT_MAX
            );

            //Start the contest
            animalContest.Start();
        }

        
    }
}
