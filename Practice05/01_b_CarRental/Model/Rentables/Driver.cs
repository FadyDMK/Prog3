﻿using System;
using System.Text;

namespace CarRent
{
    public class Driver : IRentable
    {
        public enum Licenses : byte
        {
            A = 1,
            B = 2,
            C = 4,
            D = 8,
            E = 16,
            T = 32,
            TR = 64,
            V = 128
        }

        private byte license;

        public string Name { get; private set; }

        public Driver(string name, byte license)
        {
            Name = name;
            this.license = license;
            IsAvailable = true;
        }

        #region License operations

        public bool HasLicense(Licenses needle)
        {
            return (license & (byte)needle) > 0;
        }

        public void AddLicense(Licenses newLicense)
        {
            license |= (byte)newLicense;
        }

        public void RevokeLicense(Licenses newLicense)
        {
            license &= (byte)~(byte)newLicense;
        }

        public string ListLicenses()
        {
            StringBuilder sb = new StringBuilder();
            foreach(Licenses license in Enum.GetValues(typeof(Licenses)))
            {
                sb.Append(
                    string.Format(
                        "{0}: {1}; ", license, HasLicense(license) ? "i" : "n"
                    )
                );
            }
            return sb.ToString();
        }

        #endregion

        #region IRentable interface impelementations

        public int Fare {
            get
            {
                return ((int)Math.Log(license, 2) + 1) * 12000;
            }
        }

        public bool IsAvailable { get; private set; }

        public bool Rent()
        {
            if (IsAvailable)
            {
                IsAvailable = false;
                return true;
            }
            return false;
        }

        public bool Return()
        {
            if (!IsAvailable)
            {
                IsAvailable = true;
                return true;
            }
            return false;
        }

        #endregion

        public override string ToString()
        {
            //Content of base.ToString has been extended
            return string.Format("{0} driver licenses: {1} - {2}",
                Name, ListLicenses(), IsAvailable ? "bérelhető" : "nem bérelhető");
        }
    }
}
