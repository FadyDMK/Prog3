﻿using System;

namespace Divisor
{
    class Program
    {
        private const int DIVISOR = 5;

        static void Main(string[] args)
        {
            //An integer data is required, stored in 'number' variable
            int number;

            //Reading a positive integer number from console
            Console.Write("Enter a positive number: ");
            
            //TryParse tries parsing, returns parse successfulness, parsed number is stored in number variable
            //If parse failed, repeat the input
            while (!int.TryParse(Console.ReadLine(), out number) || number <= 0)
            {
                System.Console.WriteLine("Please try again!");
            }

            //Parity check
            if (number % 2 == 0)
            {
                Console.WriteLine("The {0} number is even", number);
            }
            else
            {
                Console.WriteLine("The {0} number is odd", number);
            }

            //Parity check with conditional operator
            Console.WriteLine(number % 2 == 0 ? "The {0} number is even" : "The {0} number is odd", number);

            //Very compact parity check
            Console.WriteLine("The {0} number is {1}", number, number % 2 == 0 ? "even" : "odd");



            //Divisibility check, divisor is set as constant
            if (number % DIVISOR == 0)
            {
                Console.WriteLine("The {0} number is divisible by {1}", number, DIVISOR);
            }
            else
            {
                Console.WriteLine("The {0} number is NOT divisible by {1}", number, DIVISOR);
            }
        }
    }
}
