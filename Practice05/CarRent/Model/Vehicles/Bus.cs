﻿using System;

namespace CarRent
{
    class Bus : Vehicle
    {
        //Generic multiplier (coefficient) is stored in class property
        public static int SeatPrice { get; set; }  = 20;

        public int SeatCount { get; private set; }

        //Constructor of base class is called with received parameters
        public Bus(string licence, int prodYear, string id, int seatCount) 
            : base(licence, prodYear, id)
        {
            SeatCount = seatCount;
        }

        //Override inherited proterty to provide more specific behavior
        //Extend Fare by bus specific components
        override public int Fare
        {
            get
            {
                return base.Fare + SeatCount * SeatPrice;
            }
        }

        public override string ToString()
        {
            //Content of base.ToString has been extended
            return string.Format("Busz ({0}, ülőhely: {1}, bérleti díj: {2}Ft) {3}", 
                base.ToString(), SeatCount, Fare, IsAvailable ? "bérelhető" : "nem bérelhető");
        }
    }
}
