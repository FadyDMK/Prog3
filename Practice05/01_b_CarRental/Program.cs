﻿using CarRent.Model;
using System;
using System.Collections;

namespace CarRent
{
    class Program
    {
        private const int CURRENT_YEAR = 2022;

        static void Main(string[] args)
        {
            VehicleCatalog vehicles = new VehicleCatalog(
                new FileParser().Parse("..\\..\\jarmuvek.txt"), 
                30000
            );

            PrintCatalogItems((IEnumerable)vehicles, "Full list");

            PrintCatalogItems(vehicles.FindBusWithSeats(33), "buses with minimum 33 seats");

            PrintCatalogItems(vehicles.FindTruckWithWeight(2), "trucks with minimum 2t load");

            Console.WriteLine("----==== Find: DRS-374");
            Console.WriteLine(vehicles.Find("DRS-374"));

            Console.WriteLine("----==== Index: DRS-374");
            Console.WriteLine(vehicles["DRS-374"]);

            //Call vehicle rent related operation method with command item from enumerated list
            vehicles.Operation("DRS-374", VehicleCatalog.Commands.Rent);
            Console.WriteLine("----==== Rent: DRS-374");
            Console.WriteLine(vehicles.Find("DRS-374"));

            //Call vehicle rent related operation method with instance method reference
            vehicles.Operation("DRS-374", vehicles.Return);
            Console.WriteLine("----==== Return: DRS-374");
            Console.WriteLine(vehicles["DRS-374"]);

            Console.WriteLine("----==== Validate: DRS-374");
            //Call operation method with class method reference
            if (vehicles.Operation("DRS-374", VehicleCatalog.CheckLicenseFormat))
            {
                Console.WriteLine("Valid license");
            }
            else { 
                Console.WriteLine("Invalid license");
            }


            //==========Execute custom operations 
            Console.WriteLine("----==== Check if vehicle is new: DRS-374");
            //Call operation method with class method reference
            Console.WriteLine(vehicles.Operation("DRS-374", IsNew));

            Console.WriteLine("----==== Check if vehicle ran a little: DRS-374");
            //Call operation method with lambda expression
            Console.WriteLine(vehicles.Operation("DRS-374", v => v.Kms < 20000));


            //========================================
            // Also rent drivers and manage their driving licence
            //========================================

            //Manage driver state with enums
            Driver piquet = new Driver("Nelson Piquet", ((byte)Driver.Licenses.A)|((byte)Driver.Licenses.B));
            piquet.AddLicense(Driver.Licenses.B);
            piquet.AddLicense(Driver.Licenses.C);
            piquet.AddLicense(Driver.Licenses.D);
            Console.WriteLine(piquet.ListLicenses());
            piquet.RevokeLicense(Driver.Licenses.C);
            Console.WriteLine("Revoke license C");
            Console.WriteLine(piquet.ListLicenses());

            //Collect all rentables in a single collection
            //This can be based on IRentable interface
            RentableCatalog rentables = new RentableCatalog();
            rentables.Add(piquet);
            for (int i = 0; i < vehicles.Count; i++)
            {
                rentables.Add(vehicles[i]);
            }

            //Use a method to print the catalog
            PrintCatalog(vehicles, "Vehicles");

            //Type parameter of ICatalog is different, can not call PrintCatalog with rentables
            //PrintCatalog(rentables, "Rentables");

            //ICatalog implements IEnumerable without type parameter,
            // therefore both ICatalog<Vehicle> and ICatalog<IRentable> references can be casted to IEnumrable
            // even if they have no identical Class in the inheritance tree (except Object)
            PrintCatalogItems(vehicles, "Vehicle list");
            PrintCatalogItems(rentables, "Rentable list");
        }

        public static bool IsNew(Vehicle v)
        {
            return CURRENT_YEAR - v.ProdYear < 5;
        }

        public static void PrintCatalog(ICatalog<Vehicle> catalog, string title)
        {
            Console.WriteLine("----==== {0}", title);
            foreach (var i in catalog)
            {
                Console.WriteLine(i);
            }
        }

        //Print any cathalogs, even if they have type parameters (can not be casted)
        //This is implemented by IEnumerable interface
        public static void PrintCatalogItems(IEnumerable list, string title)
        {
            Console.WriteLine("----==== {0}", title);
            foreach(var i in list)
            {
                Console.WriteLine(i);
            }
        }
    }
}
